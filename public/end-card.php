<!DOCTYPE html>
<html lang='en'>
    <head>
        <title>TLG END CARD</title>
        <link rel="stylesheet" href="/res/css/ionicons.min.css">
        <link rel="stylesheet" href="/res/end-card.css">
    </head>
    <body>
        <countdown-timer>
            <div id='counter'>4</div>
        </countdown-timer>

        <div id='darkness'>
            <!-- This is the vignette/gradient style -->
        </div>

<?php
    // Include the Patreon API
    include "../private/vendor/autoload.php";
    include "../private/patron.php";

    // Create the 16 rows of TLG logos that scroll in the background
    echo "<tlg-rotate>";
    for($i = 0; $i <= 16; $i++){
        echo row(15);
    }
    echo "</tlg-rotate>";

    // Define the list patrons displayed on the end card:
    // Patrons in the $2 and $4 tiers don't qualify for the end card perk,
    // so we leave these as empty strings. This is important later...
    $defaults = [
        "patron32" => "\n<div class='patrons patron32'>\n<h3>$32+ Singularity Club Members</h3>\n<div class='patronFlex'>\n",
        "patron16" => "\n<div class='patrons patron16'>\n<h3>$16+ Hypervisor Contributors</h3>\n<div class='patronFlex'>\n",
        "patron8"  => "\n<div class='patrons patron8'>\n<h3>$8+ Runtime Contributors</h3>\n<div class='patronFlex'>\n",
        "patron4"  => "",
        "patron2"  => ""
    ];

    // Duplicate the strings into 
    $patreon = $defaults;

    // Loop through the patreon tiers
    foreach($fullListOfPatrons as $tier => $value){
        foreach($value as $k => $v){
            // Render out the patrons into a the HTML representation
            $patreon[$tier] .= "<h1 class='patron' title='$" . ($v[1]/100) . " $tier'>".$v[0]."</h1>\n";
        }
    }

    // Define the credits:
    $credits  = "\n<div class='credits fade-out'>";
    $credits .= "\n  <div class='credits-container'>";
    $credits .= "\n    <div class='credits1'>";
    $credits .= "\n       <h4>Created By</h4>";
    $credits .= "\n       <h1>Gardiner Bryant</h1>";
    $credits .= "\n       <h4>With help from my generous</h4>";
    $credits .= "\n       <h1>Patreon Supporters</h1>";
    $credits .= "\n       <h5>And an extra special thanks to...</h5>";
    
    // Add the patron32 tier to the document:
    $credits .= $patreon["patron32"] . "</div></div>";
    $credits .= "\n    </div><br><br>";
    $credits .= "\n   <div class='credits2'>";

    // Check if we need to add the $16 tier since there are sometimes
    // no patrons in this class
    if($patreon['patron16'] !== $defaults['patron16']){
        // This is why we needed to duplicate the defaults so we could
        // check if they'd been modified ;)
        $credits .= "\n      " . $patreon["patron16"] . "</div></div><br><br>";
    }

    // Add the patron8 tier to the documents
    $credits .= "\n      " . $patreon["patron8"] . "</div></div><br><br>";
    
    // Secret sauce:
    if(isset($_GET['token'])){
        $credits .= "<h5 style='text-transform:none;'> " . htmlspecialchars($_GET['token']) . "</h5><br>";
    }

    // End of the credits:
    $credits .= "\n      </div>"; 
    $credits .= "\n   </div>"; // Close .credits-container
    $credits .= "\n</div>"; // End of the credits/fade-out class


    /* ============== Define the "elsewhere" stuff ============== */

    // We do this in PHP so that we can duplicate this block and create a stroke
    // around the text which is BEHIND the #darkness vignette
    $elsewhere  = "\n<div class='elsewhere'>";
    $elsewhere .= "\n   <h3>Join me elsewhere on the web</h3>";
    $elsewhere .= "\n   <div class='elsewhereItem heavyelement'>https://heavyelement.io/</div>";
    $elsewhere .= "\n   <div class='elsewhereItem offtopical'>https://offtopical.net/</div>";
    $elsewhere .= "\n   <div class='elsewhereItem twitch'>https://twitch.tv/xondak/</div>";
    $elsewhere .= "\n   <div class='elsewhereItem twitter'>@TheLinuxGamer</div>";
    $elsewhere .= "\n   <div class='elsewhereItem librem'>gbryant@librem.one</div>";
    $elsewhere .= "\n</div>";

    // Insert all the things we've created in PHP into the actual HTML:
    echo "$credits\n$credits\n$elsewhere\n$elsewhere\n";
?>
        
        <div id='interval'>-3</div><!-- Show how many seconds have passed since the countdown was finished -->
        
        <div id='merch'>
            <div class='shirt' id='tlg'></div>
            <div class='shirt' id='openSource'></div>
            <div class='shirt' id='foss'></div>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>

        <script src='/res/tlg.js'></script>
    </body>
</html>

<?php

function row($count){
    // This function outputs a row of TLG elements with a random
    // CSS animation duration so they move at slightly different rates
    $logo = "<tlg-logo></tlg-logo>";
    $row = "<tlg-row style='animation-duration:".rand(12,30)."s;'>";
    for($i = 0; $i <= $count; $i++){
        $row .= $logo;
    }
    return $row . "</tlg-row>";
}


// For debugging only:
// echo "<script type='text/json'>".json_encode($fullListOfPatrons,JSON_PRETTY_PRINT). "</script>";